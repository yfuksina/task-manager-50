package ru.tsc.fuksina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
