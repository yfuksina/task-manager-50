package ru.tsc.fuksina.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
